import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:oxko/app/AppHelper.dart';
import 'package:oxko/firebase/FirebaseHelper.dart';
import 'package:oxko/navigation/BottomNavigationPage.dart';

const appColor = Color.fromARGB(255, 15, 190, 124);
const primaryColor = Colors.green;
const cursorColor = Colors.white;

const firebaseTopic = 'oxko';

class OxkoApp extends StatefulWidget {
  FirebaseMessaging firebaseMessaging;

  void subscribeToPushNotifications() {
    firebaseMessaging.subscribeToTopic(firebaseTopic);
  }

  void unsubscribeFromPushNotifications() {
    firebaseMessaging.unsubscribeFromTopic(firebaseTopic);
  }

  OxkoApp() {
    FlutterCrashlytics().initialize();
    firebaseMessaging = FirebaseMessaging();
    firebaseMessaging.requestNotificationPermissions();
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
      },
      onBackgroundMessage: oxkoBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    isPushNotificationsEnabled().then((enabled) {
      subscribeToPushNotifications();
    });
  }

  @override
  OxkoAppState createState() => OxkoAppState();

  static OxkoApp instance;
  static OxkoApp getInstance() {
    if (instance == null) {
      instance = OxkoApp();
    }
    return instance;
  }
}

class OxkoAppState extends State<OxkoApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          cursorColor: cursorColor,
          cupertinoOverrideTheme: CupertinoThemeData(
            primaryColor: cursorColor,
          ),
          primaryColor: primaryColor,
          primarySwatch: primaryColor,
          buttonColor: primaryColor,
          accentColor: primaryColor,
          appBarTheme: AppBarTheme(
            color: appColor,
          )),
      home: BottomNavigationPage(),
    );
  }
}
