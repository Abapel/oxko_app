import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  @override
  SplashPageStage createState() => SplashPageStage();
}

class SplashPageStage extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Splash'),
      ),
    );
  }
}
