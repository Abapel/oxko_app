import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NavigationControls extends StatefulWidget {
  NavigationControls(this._webViewControllerFuture)
      : assert(_webViewControllerFuture != null);

  final Future<WebViewController> _webViewControllerFuture;
  NavigationControlsState state;

  @override
  NavigationControlsState createState() {
    state = NavigationControlsState(_webViewControllerFuture);
    return state;
  }

  void updateButtons() {
    state.updateButtonsAsync();
  }
}

// ignore: must_be_immutable
class NavigationControlsState extends State<NavigationControls> {
  NavigationControlsState(this._webViewControllerFuture)
      : assert(_webViewControllerFuture != null);

  final Future<WebViewController> _webViewControllerFuture;

  WebViewController webController;
  bool canGoBack = false;
  bool canGoForward = false;

  void updateButtonsAsync() {
    updateButtons().then((result) {
      setState(() {});
    });
  }

  Future<void> updateButtons() async {
    if (webController != null) {
      canGoBack = await webController.canGoBack();
      canGoForward = await webController.canGoForward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: _webViewControllerFuture,
      builder:
          (BuildContext context, AsyncSnapshot<WebViewController> snapshot) {
        final bool webViewReady =
            snapshot.connectionState == ConnectionState.done;
        webController = snapshot.data;
        return Row(
          children: <Widget>[
            Offstage(
              offstage: !canGoBack,
              child: IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: !webViewReady
                    ? null
                    : () => navigate(context, webController, goBack: true),
              ),
            ),
            Offstage(
              offstage: !canGoForward,
              child: IconButton(
                icon: const Icon(Icons.arrow_forward_ios),
                onPressed: !webViewReady
                    ? null
                    : () => navigate(context, webController, goBack: false),
              ),
            ),
          ],
        );
      },
    );
  }

  navigate(BuildContext context, WebViewController controller,
      {bool goBack: false}) async {
    bool canNavigate =
        goBack ? await controller.canGoBack() : await controller.canGoForward();
    if (canNavigate) {
      goBack ? controller.goBack() : controller.goForward();
    }
  }
}
