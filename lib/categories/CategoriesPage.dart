import 'package:flutter/material.dart';
import 'package:oxko/categories/Category.dart';
import 'package:oxko/web/WebPage.dart';

class CategoriesPage extends StatefulWidget {
  @override
  CategoriesPageState createState() => CategoriesPageState();
}

class CategoriesPageState extends State<CategoriesPage> {
  List<Category> categories = <Category>[
    Category(title: 'Авто', url: 'https://oxko.ru/avto/'),
    Category(title: 'Гаджеты', url: 'https://oxko.ru/gadgety/'),
    Category(
        title: 'Красота и здоровье', url: 'https://oxko.ru/krasota-i-zdorove/'),
    Category(title: 'Мультимедиа', url: 'https://oxko.ru/multimedia/'),
    Category(
        title: 'Планшеты и ноутбуки',
        url: 'https://oxko.ru/planshety-noutbuki/'),
    Category(title: 'Полезное', url: 'https://oxko.ru/poleznoe/'),
    Category(title: 'Смартфоны', url: 'https://oxko.ru/smartfony/'),
    Category(
        title: 'Техника для дома', url: 'https://oxko.ru/tehnika-dlya-doma/'),
  ];

  void onCategoryClicked(Category category) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => WebPage(category)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Категории'),
      ),
      body: ListView.builder(
          itemCount: categories.length,
          itemBuilder: (context, i) {
            return ListTile(
              title: Text(categories[i].title),
              onTap: () {
                onCategoryClicked(categories[i]);
              },
            );
          }),
    );
  }
}
