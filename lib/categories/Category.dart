class Category {
  final String title;
  final String url;

  Category({this.title, this.url});

  static Category main = Category(title: 'Главная', url: 'https://oxko.ru/');
}
