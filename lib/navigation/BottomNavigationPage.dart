import 'package:flutter/material.dart';
import 'package:oxko/categories/CategoriesPage.dart';
import 'package:oxko/categories/Category.dart';
import 'package:oxko/search/SearchPage.dart';
import 'package:oxko/settings/SettingsPage.dart';
import 'package:oxko/web/WebPage.dart';

class BottomNavigationPage extends StatefulWidget {
  @override
  BottomNavigationPageState createState() => BottomNavigationPageState();
}

class BottomNavigationPageState extends State<BottomNavigationPage> {
  static WebPage homePage = WebPage(Category.main);
  static CategoriesPage categoriesPage = CategoriesPage();
  static SearchPage searchPage = SearchPage();
  static SettingsPage settingsPage = SettingsPage();

  static int selectedIndex = 0;
  List<BottomNavigationBarItem> bottomBarItems;
  List<Widget> pages;

  List<BottomNavigationBarItem> getBottomBarItems() {
    return <BottomNavigationBarItem>[
      BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('1')),
      BottomNavigationBarItem(icon: Icon(Icons.list), title: Text('2')),
      BottomNavigationBarItem(icon: Icon(Icons.search), title: Text('3')),
      BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text('4')),
    ];
  }

  List<Widget> getPages() {
    return <Widget>[homePage, categoriesPage, searchPage, settingsPage];
  }

  Widget buildOffstageNavigator(int index) {
    return Offstage(offstage: index != selectedIndex, child: pages[index]);
  }

  void onBottomBarItemClicked(int index) {
    if (!mounted) return;
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    bottomBarItems = getBottomBarItems();
    pages = getPages();

    return Scaffold(
      body: Stack(
        children: <Widget>[
          buildOffstageNavigator(0),
          buildOffstageNavigator(1),
          buildOffstageNavigator(2),
          buildOffstageNavigator(3),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: getBottomBarItems(),
        currentIndex: selectedIndex,
        onTap: onBottomBarItemClicked,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedItemColor: Theme.of(context).accentColor,
        unselectedItemColor: Colors.grey[400],
        selectedLabelStyle: TextStyle(color: Theme.of(context).accentColor),
        unselectedLabelStyle: TextStyle(color: Colors.grey[600]),
      ),
    );
  }
}
