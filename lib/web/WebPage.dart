import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oxko/categories/Category.dart';
import 'package:oxko/widgets/NavigationControls.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebPage extends StatefulWidget {
  final Category category;
  WebPage(this.category);

  @override
  WebPageState createState() => WebPageState(category);
}

class WebPageState extends State<WebPage> {
  final Category category;
  WebPageState(this.category);

  Completer<WebViewController> webController = Completer<WebViewController>();
  NavigationControls navigationControls;

  @override
  Widget build(BuildContext context) {
    navigationControls = NavigationControls(webController.future);

    return Scaffold(
      appBar: AppBar(
        title: Text(category.title),
        actions: <Widget>[navigationControls],
      ),
      body: WebView(
        initialUrl: category.url,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          webController.complete(webViewController);
        },
        onPageFinished: (String s) {
          navigationControls.updateButtons();
        },
      ),
    );
  }
}
