import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'package:oxko/app/AppHelper.dart';
import 'package:oxko/app/OxkoApp.dart';
import 'package:share/share.dart';

class SettingsPage extends StatefulWidget {
  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {
  bool checked = false;

  @override
  void initState() {
    super.initState();

    isPushNotificationsEnabled().then((enabled) {
      setState(() {
        checked = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Настройки'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          ListTile(
            title: Row(
              children: <Widget>[Text('Поделиться приложением')],
            ),
            onTap: () {
              Share.share(
                  'https://play.google.com/store/apps/details?id=com.oxko.oxko');
            },
          ),
          ListTile(
            title: Row(
              children: <Widget>[Text('Оценить в Google Play')],
            ),
            onTap: () {
              LaunchReview.launch();
            },
          ),
          CheckboxListTile(
            title: Row(
              children: <Widget>[Text('Показывать уведомления')],
            ),
            value: checked,
            onChanged: (bool value) {
              setPushNotificationsEnabled(value);
              setState(() {
                checked = value;
                if (checked) {
                  OxkoApp.getInstance().subscribeToPushNotifications();
                } else {
                  OxkoApp.getInstance().unsubscribeFromPushNotifications();
                }
              });
            },
          ),
        ],
      ),
    );
  }
}
