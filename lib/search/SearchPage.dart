import 'package:flutter/material.dart';
import 'package:oxko/app/AppHelper.dart';
import 'package:oxko/app/OxkoApp.dart';
import 'package:oxko/categories/Category.dart';
import 'package:oxko/web/WebPage.dart';

class SearchPage extends StatefulWidget {
  @override
  SearchPageState createState() => SearchPageState();
}

class SearchPageState extends State<SearchPage> {
  final searchController = TextEditingController();
  bool searched = false;

  String getSearchUrl() {
    String s = 'https://oxko.ru/?s=${searchController.text}';
    return s;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: searched
          ? null
          : AppBar(
              title: Text('Поиск по сайту'),
            ),
      body: Stack(
        children: <Widget>[
          Offstage(
            offstage: searched,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  TextField(
                    onChanged: (value) {
                      print(value);
                    },
                    decoration: InputDecoration(
                      hasFloatingPlaceholder: false,
                      labelText: 'Поисковый запрос',
                    ),
                    textInputAction: TextInputAction.next,
                    controller: searchController,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    'Для поиска по сайту введите поисковый запрос и нажмите кнопку Найти',
                    style: TextStyle(fontSize: 12, color: Colors.grey),
                    textAlign: TextAlign.center,
                  ),
                  FlatButton(
                    child: Text(
                      'Найти',
                      style: TextStyle(fontSize: 20, color: appColor),
                    ),
                    onPressed: () async {
                      setState(() {
                        hideKeyboard(context);
                        searched = true;
                      });
                    },
                  )
                ],
              ),
            ),
          ),
          Offstage(
            offstage: !searched,
            child: searched
                ? WebPage(
                    Category(title: 'Результаты поиска', url: getSearchUrl()))
                : null,
          )
        ],
      ),
    );
  }
}
